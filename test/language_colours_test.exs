defmodule LanguageColoursTest do
  use ExUnit.Case

  setup_all do
    start_supervised!(
      {LanguageColours.ETSDatabase,
       LanguageColours.ETSDatabase.startup_options(
         Application.get_env(:language_colours, :databases).test_db
       )}
    )

    :ok
  end

  test "gets the correct colour" do
    assert LanguageColours.get("Ada", :test_db) == "#02f88c"
  end

  test "gets fallback if colour doesn't exist" do
    assert "#" <> <<_rest::binary-size(6)>> = LanguageColours.get("Aaaaaaadaaaaaaa", :test_db)
  end

  test "is case insensitive" do
    assert LanguageColours.get("AdA", :test_db) == "#02f88c"
  end

  test "returns nil if there is no fallback and language wasn't found" do
    config =
      Application.get_env(:language_colours, :databases).test_db
      |> Map.put(:fallback, false)
      |> Map.put(:server_name, :test_db_2)
      |> Map.put(:ets_table, :best_table)

    start_supervised!(
      {LanguageColours.ETSDatabase, LanguageColours.ETSDatabase.startup_options(config)}
    )

    assert is_nil(LanguageColours.get("Aaaaaaadaaaaaaa", config))
  end

  test "language is deleted if no longer in updated dataset" do
    config =
      Application.get_env(:language_colours, :databases).test_db
      |> Map.put(:file, "test/colours-without-ada.json")

    assert LanguageColours.update(config) == :ok
    assert LanguageColours.get("Ada", config) != "#02f88c"

    # Teardown
    config = Application.get_env(:language_colours, :databases).test_db
    assert LanguageColours.update(config) == :ok
  end

  test "languages are updated correctly" do
    config =
      Application.get_env(:language_colours, :databases).test_db
      |> Map.put(:file, "test/colours-without-ada.json")

    assert LanguageColours.get("1C Enterprise", config) == "#814CCC"
    assert LanguageColours.update(config) == :ok
    assert LanguageColours.get("Ada", config) != "#ABCDEF"

    # Teardown
    config = Application.get_env(:language_colours, :databases).test_db
    assert LanguageColours.update(config) == :ok
  end
end
