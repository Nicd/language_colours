defmodule LanguageColours.Colour do
  @typedoc """
  A colour returned by the API, a hex colour of six characters with a hash prepended.
  """
  @type t :: String.t()
end
