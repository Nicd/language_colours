defmodule Mix.Tasks.Language.Colours.Dl.Data do
  use Mix.Task

  @moduledoc """
  Download the GitHub colours JSON dataset (or any other specified dataset).

  **NOTE**: This function does not check the HTTPS certificate! Check the file after downloading
  and only use this task as a helper during development.

  You can specify the download URL and file path to put the download to with flags:

  ```bash
  mix language.colours.dl.data --url 'https://raw.githubusercontent.com/ozh/github-colors/master/colors.json' --path 'priv/language_colours/data.json'
  ```

  The directories leading to the output file will be created if necessary.
  """

  @default_url "https://raw.githubusercontent.com/ozh/github-colors/master/colors.json"

  @doc false
  @impl true
  def run(argv) do
    {opts, _, _} = OptionParser.parse(argv, strict: [url: :string, path: :string])

    url = opts |> Keyword.get(:url, @default_url) |> String.to_charlist()
    path = Keyword.get(opts, :path, Path.join(File.cwd!(), "colours.json"))

    Application.ensure_all_started(:inets)
    Application.ensure_all_started(:ssl)

    {:ok, {_, _, data}} = :httpc.request(:get, {url, []}, [], [])
    path |> Path.dirname() |> File.mkdir_p!()
    File.write!(path, data)
  end
end
