defmodule LanguageColours.Database do
  @moduledoc """
  Behaviour that must be implemented by a database.

  Each function gets a database configuration as argument. This configuration should be used to
  talk to the correct database instance.

  It is not necessary to deal with the fallback as the main module handles that.
  """

  @doc """
  Get a colour for the given language, or `nil`.
  """
  @callback get(language :: String.t(), db_config :: map()) :: LanguageColours.Colour.t() | nil

  @doc """
  Update the language dataset using the configuration.
  """
  @callback update(db_config :: map()) :: :ok
end
