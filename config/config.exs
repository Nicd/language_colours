import Config

config :language_colours, json_parser: Jason

if Config.config_env() == :test do
  import_config("test.exs")
end
