import Config

config :language_colours,
  databases: %{
    test_db: %{
      db: LanguageColours.ETSDatabase,
      ets_table: :lc_ets_test,
      file: "test/colours.json",
      server_name: TestETSDatabase,
      fallback: true
    }
  }
