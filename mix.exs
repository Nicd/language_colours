defmodule LanguageColours.MixProject do
  use Mix.Project

  def project do
    [
      app: :language_colours,
      version: "1.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      name: "LanguageColours",
      source_url: "https://gitlab.com/Nicd/language_colours",
      homepage_url: "https://gitlab.com/Nicd/language_colours",
      docs: [
        # The main page in the docs
        main: "LanguageColours"
      ],
      description:
        "LanguageColours offers an API for retrieving colours for programming languages based on GitHub colour data (or some other dataset).",
      package: package()
    ]
  end

  defp package do
    [
      name: :language_colours,
      maintainers: ["Mikko Ahlroth <mikko@ahlroth.fi>"],
      licenses: ["BSD 3-clause"],
      links: %{
        "GitLab" => "https://gitlab.com/Nicd/language_colours"
      }
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.24", only: :dev, runtime: false},
      {:rainbow, "~> 0.1.0", optional: true},
      {:jason, "~> 1.2", optional: true}
    ]
  end
end
